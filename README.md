# README #

### RUN API ###
In order to start the app.
Run the command docker-compose up inside api-flak folder.
The port is the 5000

The end-point by default is the one for the task so to prove the api just
send a request to: localhost:5000/
It must be a POST request with Header = {Content-Type: application/json}
Body = {"sentence": "President Xi Jinping warned of the epidemic escalating outside the epicentre of Hubei province as more people travel and crowds gather across the country."}

### RUN TESTCASES ###
For run the testcases just execute the next command:
python3 -m pytest tests/test_api.py

