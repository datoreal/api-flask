from flask import Flask, request, jsonify, make_response

import spacy

nlp = spacy.load('en_core_web_sm')

api_app = Flask(__name__)


@api_app.route('/', methods=['POST'])
def index():
    if request.method == 'POST':
        try:
            input_data = request.get_json()
        except:
            return {'error': 'Bad request there body is not json format'}, 400
        try:
            s = input_data['sentence']
        except KeyError as err:
            return {'error': str(err)}, 400
        doc = nlp(s)
        output = []
        for ent in doc.ents:
            if ent.text and ent.label_:
                output.append((ent.text, ent.label_))
        return make_response(jsonify(named_entities=output), 200)
    return {'error': 'Only POST petitions allowed'}, 405


if __name__ == '__main__':
    api_app.run(host='0.0.0.0')
