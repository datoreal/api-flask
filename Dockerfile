FROM python:3.6
ENV APP /app
RUN mkdir /code
WORKDIR /code
EXPOSE 5000
ADD requirements.txt /code/
RUN pip install -r requirements.txt
RUN python -m spacy download en_core_web_sm
ADD . /code/
CMD [ "uwsgi", "--ini", "app.ini" ]