from app import api_app

import unittest
import json


class TestApi(unittest.TestCase):
    def setUp(self):
        self.app = api_app.test_client()

    def test_wrong_type_request(self):
        res = self.app.get('/',
                           content_type='application/json')
        assert res.status_code == 405

    def test_empty_body(self):
        expected_output = {'error': 'Bad request there body is not json format'}
        res = self.app.post('/',
                            content_type='application/json')
        assert res.get_json() == expected_output
        assert res.status_code == 400

    def test_empty_sentence(self):
        s = ''
        expected_output = {"named_entities": []}
        res = self.app.post('/',
                            data=json.dumps(dict(sentence=s)),
                            content_type='application/json')
        assert res.get_json() == expected_output
        assert res.status_code == 200

    def test_sentence_no_entities(self):
        s = 'This is a test'
        expected_output = {"named_entities": []}
        res = self.app.post('/',
                            data=json.dumps(dict(sentence=s)),
                            content_type='application/json')
        assert res.get_json() == expected_output
        assert res.status_code == 200

    def test_sentence_no_exist(self):
        expected_output = {"error": "'sentence'"}
        res = self.app.post('/',
                            data=json.dumps(dict()),
                            content_type='application/json')
        assert res.get_json() == expected_output
        assert res.status_code == 400

    def test_sentence(self):
        s = "President Xi Jinping warned of the epidemic escalating outside the epicentre of Hubei province as more people travel and crowds gather across the country."
        expected_output = {"named_entities":[["Xi Jinping","PERSON"],["Hubei","GPE"]]}
        res = self.app.post('/',
                            data=json.dumps(dict(sentence=s)),
                            content_type='application/json')
        assert res.get_json() == expected_output
        assert res.status_code == 200

    def test_sentence_join_names(self):
        s = "President Xi JinpingHubei province as more people travel and crowds gather across the country."
        expected_output = {"named_entities": [["Xi JinpingHubei", "PERSON"]]}
        res = self.app.post('/',
                            data=json.dumps(dict(sentence=s)),
                            content_type='application/json')
        assert res.get_json() == expected_output
        assert res.status_code == 200


if __name__ == '__main__':
    unittest.main()
